package sample.ibatisconsole.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import sample.ibatisconsole.base.BaseDao;
import sample.ibatisconsole.model.ADM.EmployeeModel;

/**
 *
 * @author arif
 */
public interface EmployeeDao extends BaseDao<EmployeeModel>{
    public EmployeeModel getEmployee(@Param("employeeId") String employeeId, @Param("employeeName")String employeeName);
}
