package sample.ibatisconsole.main;

import java.util.List;
import sample.ibatisconsole.dao.EmployeeDao;
import sample.ibatisconsole.daoImpl.EmployeeDaoImpl;
import sample.ibatisconsole.model.ADM.EmployeeModel;
import sample.ibatisconsole.util.IbatisUtil;
import sample.ibatisconsole.util.JdbcConfig;

/**
 *
 * @author arif
 */
public class MainClass {

    public static void main(String[] args) {
        EmployeeDao manager = new EmployeeDaoImpl();
        EmployeeModel employee1 = new EmployeeModel();

        
        //System.out.println("Connection URL = "+JdbcConfig.getConnectionUrl());
        
        String empId = "KONTINUM22\\arif";
        String empName = "Arif Febriasyah";
        employee1.setEmployeeId(empId);
        employee1.setEmployeeName(empName);
        employee1.setAddress("Kalibata Timur");
        employee1.setPhone("085710903686");
        employee1.setEmail("arif.febr@gmail.com");
        employee1.setGender('M');
        manager.save(employee1);

        System.out.println("=========== Get Employee By Id ===========");
        EmployeeModel emp1 = manager.getById(empId);
        System.out.println(emp1.getEmployeeId() + " - " + emp1.getEmployeeName());

        System.out.println("=========== Get List Employee ===========");
        List<EmployeeModel> allEmp = manager.getAll();
        for (EmployeeModel employee : allEmp) {
            System.out.println(employee.getEmployeeId() + " - " + employee.getEmployeeName());
        }

        System.out.println("=========== Get Employee By Id AND Name ===========");
        EmployeeModel emp2 = manager.getEmployee(empId, empName);
        System.out.println("<><><>" + emp2.getEmployeeId() + " - " + emp2.getEmployeeName());

    }
}
