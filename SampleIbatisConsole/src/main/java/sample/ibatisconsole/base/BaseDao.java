package sample.ibatisconsole.base;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author arif
 * @param <T> as Model
 */
public interface BaseDao<T> {
    public void save(T data);
    public void update(T data);
    public void delete(T data);
    public void deleteById(Serializable id);
    public T getById(Serializable id);
    public List<T> getAll();
}
