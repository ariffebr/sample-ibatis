package sample.ibatisconsole.daoImpl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import sample.ibatisconsole.dao.EmployeeDao;
import sample.ibatisconsole.model.ADM.EmployeeModel;
import sample.ibatisconsole.util.IbatisUtil;

/**
 *
 * @author arif
 */
public class EmployeeDaoImpl implements EmployeeDao {

    @Override
    public void save(EmployeeModel data) {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            try {
                sqlSession.insert("ADM_Employee.save", data);
                sqlSession.commit();
            } catch (Exception ex) {
                sqlSession.rollback();
                throw ex;
            }
        }
    }

    @Override
    public void update(EmployeeModel data) {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            try {
                sqlSession.update("ADM_Employee.update", data);
                sqlSession.commit();
            } catch (Exception ex) {
                sqlSession.rollback();
                throw ex;
            }
        }
    }

    @Override
    public void delete(EmployeeModel data) {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            try {
                sqlSession.delete("ADM_Employee.delete", data);
                sqlSession.commit();
            } catch (Exception ex) {
                sqlSession.rollback();
                throw ex;
            }
        }
    }

    @Override
    public void deleteById(Serializable id) {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            try {
                sqlSession.delete("ADM_Employee.delete", id);
                sqlSession.commit();
            } catch (Exception ex) {
                sqlSession.rollback();
                throw ex;
            }
        }
    }

    @Override
    public EmployeeModel getById(Serializable id) {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            return (EmployeeModel) sqlSession.selectOne("ADM_Employee.getById", id);
        }
    }

    @Override
    public List<EmployeeModel> getAll() {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            return sqlSession.selectList("ADM_Employee.getAll");
        }
    }

    @Override
    public EmployeeModel getEmployee(@Param("employeeId") String employeeId, @Param("employeeName") String employeeName) {
        try (SqlSession sqlSession = IbatisUtil.getSqlSessionFactory().openSession()) {
            Map<String, String> keys = new HashMap<>();
            keys.put("employeeId", employeeId);
            keys.put("employeeName", employeeName);

            return (EmployeeModel) sqlSession.selectOne("ADM_Employee.getEmployee", keys);
        }
    }

}
