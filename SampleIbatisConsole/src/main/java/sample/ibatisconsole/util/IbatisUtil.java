package sample.ibatisconsole.util;

import java.io.IOException;
import java.io.Reader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 *
 * @author arif
 */
public class IbatisUtil {

    private static JdbcConfig jdbcConfig;
    private static SqlSessionFactory sessionFactory;

    private IbatisUtil() {
    }

    static {

        try {
            final String jdbcConfigLocation = "Configs/JdbcConfig.xml";

            Properties ibatisConfigProperties = Resources.getResourceAsProperties(jdbcConfigLocation);
            System.out.println("aaaa => "+ibatisConfigProperties.getProperty("#{JDBC.Password}"));
            
            Reader reader = Resources.getResourceAsReader(jdbcConfigLocation);

            sessionFactory = new SqlSessionFactoryBuilder().build(reader);

            if (sessionFactory != null) {
                Properties prop = sessionFactory.getConfiguration().getVariables();
            }

            JdbcConfig.setDriver(ibatisConfigProperties.getProperty("JDBC.Driver"));
            JdbcConfig.setConnectionUrl(ibatisConfigProperties.getProperty("JDBC.ConnectionURL"));
            JdbcConfig.setDbUserName(ibatisConfigProperties.getProperty("JDBC.Username"));
            JdbcConfig.setDbPassword(ibatisConfigProperties.getProperty("JDBC.Password"));

        } catch (IOException ex) {
            Logger.getLogger(IbatisUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static JdbcConfig getJdbcConfig() {
        return jdbcConfig;
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        return sessionFactory;
    }
}
