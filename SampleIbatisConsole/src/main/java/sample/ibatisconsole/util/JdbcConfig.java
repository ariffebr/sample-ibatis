package sample.ibatisconsole.util;


/**
 *
 * @author arif
 */
public class JdbcConfig {
    private static String driver;
    private static String connectionUrl;
    private static String username;
    private static String password;

    private JdbcConfig() {
    }

    public static String getDriver() {
        return driver;
    }

    static void setDriver(String driver) {
        JdbcConfig.driver = driver;
    }    
    
    public static String getDbUserName() {
        return username;
    }

    static void setDbUserName(String dbUserName) {
        username = dbUserName;
    }

    public static String getDbPassword() {
        return password;
    }

    static void setDbPassword(String dbPassword) {
        password = dbPassword;
    }

    public static String getConnectionUrl() {
        return connectionUrl;
    }

    static void setConnectionUrl(String dbConnectionUrl) {
        connectionUrl = dbConnectionUrl;
    }        
}
